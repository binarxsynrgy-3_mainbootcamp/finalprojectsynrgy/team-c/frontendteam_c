import { Popover } from '@headlessui/react';
import { useState } from 'react';
import { XIcon } from '@heroicons/react/solid';
import Image from 'next/image';

export default function PopUpSuccess(props) {
  const [check, setCheck] = useState(props.check);
  return (
    <Popover>
      <Popover.Panel className="fixed overflow-auto md:overflow-hidden inset-0 z-20 bg-white text-center" static={check}>
        <div className="text-right mr-3 md:mr-20 text-2xl">
          <button type="button" className="text-right mt-3 md:mt-5" onClick={() => setCheck(!check)}><XIcon className="h-10 w-10" /></button>
        </div>
        <div className="-mt-10">
          <Image src={props.pic} alt="success" height={320} width={320} />
        </div>
        <div className="md:my-6 my-2">
          <h3 className="md:text-3xl text-lg font-medium">{props.title}</h3>
          <p className="mt-12 mb-4 text-base md:text-xl">Tunggu hingga 24 jam</p>
          <p className="md:text-xl text-base">{props.content}</p>
        </div>
        <div>
          <button type="button" className="mb-3 hover:bg-green-750 bg-green-650 px-3 text-white w-11/12 md:w-1/2 py-3 rounded-lg" onClick={() => setCheck(!check)}><p className="md:text-xl text-sm font-semibold md:font-bold">Ke Daftar Campaign</p></button>
        </div>
      </Popover.Panel>
    </Popover>
  );
}
