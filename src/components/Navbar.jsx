import { Popover, Transition } from '@headlessui/react';
import { Fragment, useState } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import { LogoutIcon, MenuIcon, XIcon } from '@heroicons/react/outline';
import Link from 'next/link';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Navbar(props) {
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const logOut = () => {
    localStorage.removeItem('user');
    router.replace('/login');
  };
  return (
    <div className="bg-white z-10 sticky top-0 min-w-full filter drop-shadow-xl">
      <div className="px-2 flex justify-start items-center">
        <div className="m-4 md:hidden" onClick={() => setOpen(!open)}>
          {open
            ? <XIcon className="h-5 w-5" />
            : <MenuIcon className="h-5 w-5" />}
        </div>
        <div className="mx-auto w-max pr-28 md:pr-0 md:mx-0">
          <Link href="/mahasiswa">
            <a>
              <div className="flex ml-10 items-center">
                <Image src="/Logo24x24.png" layout="fixed" width={24} height={24} />
                <p className="text-green-650">tobantoo</p>
              </div>
            </a>
          </Link>
        </div>
        <div className="md:flex hidden">
          {props.itemNav.map((item, position) => (
            <div className="py-2 pl-12" key={position}>
              <a
                key={item.name}
                href={item.href}
                className={classNames(
                  (props.page === item.name) ? 'text-green-650' : 'text-green-650 hover:text-green-750',
                  'block font-medium text-lg my-2 w-max',
                )}
                aria-current={(props.page === item.name) ? 'page' : undefined}
              >
                <p className={classNames(
                  (props.page === item.name) ? 'border-b-2 border-green-650' : '',
                  'pb-1',
                )}
                >
                  {item.name}
                </p>
              </a>
            </div>
          ))}
        </div>
        <div className="ml-auto mr-32 md:block hidden">
          <Popover className="relative">
            {({ open }) => (
              <>
                <Popover.Button className="flex">
                  <Image src="/images/student.png" className="rounded-full" width={24} height={24} />
                  <span className="ml-4">{props.username}</span>
                </Popover.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1"
                >
                  <Popover.Panel>
                    <div className="cursor-pointer absolute top-8 left-14 rounded py-1 px-3 bg-green-650 hover:bg-green-750 items-center justify-center text-white flex" onClick={logOut}>
                      <LogoutIcon className="w-5 h-5 mr-2" />
                      <a>
                        Logout
                      </a>
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
        </div>
      </div>
      <Transition
        show={open}
        enter="transition-opacity duration-75"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity duration-150"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
        className="absolute text-white bg-gray-800 md:hidden w-full h-screen"
      >
        <div className="flex mx-12 items-center my-3">
          <Image src="/images/student.png" className="rounded-full" width={40} height={40} />
          <span className="ml-4">{props.username}</span>
        </div>
        <div className="mx-12 my-5 text-lg">
          <Link href="/mahasiswa/campaign">
            <a>
              <p className="p-2">Campaign</p>
            </a>
          </Link>
          <hr className="w-full mt-2 h-1px bg-gray-800 opacity-50" />
        </div>
        <div className="mx-12 my-5 text-lg">
          <Link href="/mahasiswa/payment">
            <a>
              <p className="p-2">Dana</p>
            </a>
          </Link>
          <hr className="w-full my-auto mt-2 h-1px bg-gray-800 opacity-50" />
        </div>
        <div className="h-full mx-12 text-xl flex flex-col justify-center">
          <div onClick={logOut} className="p-2 flex items-center">
            <LogoutIcon className="w-7 h-7 mr-2" />
            Logout
          </div>
          <hr className="w-full mt-2 h-1px bg-gray-800 opacity-50" />
        </div>
      </Transition>
    </div>
  );
}
