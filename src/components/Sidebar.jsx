import { Transition } from '@headlessui/react';
import { useState } from 'react';
import { ChevronDoubleLeftIcon, UserCircleIcon } from '@heroicons/react/solid';
import { LogoutIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Sidebar(props) {
  const router = useRouter();
  const [open, setOpen] = useState(true);
  const openBar = () => {
    setOpen(!open);
  };

  const logOut = () => {
    localStorage.removeItem('user');
    router.replace('/login');
  };

  return (
    <>
      <Transition
        show={open}
        enter="transition ease-in-out duration-200 transform"
        enterFrom="-translate-x-full"
        enterTo="translate-x-0"
        leave="transition ease-in-out duration-300 transform"
        leaveFrom="translate-x-0"
        leaveTo="-translate-x-full"
        className="md:w-1/3 h-screen bg-gray-900 md:sticky top-0"
      >
        <div className="">
          <div className="mt-16 mx-auto mb-24 w-16 h-16 flex justify-center text-green-750 items-center rounded-full bg-white">
            <UserCircleIcon />
          </div>
          <div className="text-center text-gray-400 -mt-20 mb-12">
            <p>Admin Tobantoo</p>
          </div>
          <div>
            {props.itemNav.map((item) => (
              <div className="flex">
                <div className={classNames(
                  (props.page === item.name) ? 'bg-green-650' : 'hidden',
                  'w-1 h-16',
                )}
                />
                <a
                  key={item.name}
                  href={item.href}
                  className={classNames(
                    (props.page === item.name) ? 'text-green-750 bg-gray-800' : 'text-gray-500 hover:bg-gray-800 hover:text-white',
                    'block px-5 py-2 font-medium flex items-center py-4 w-full',
                  )}
                  aria-current={(props.page === item.name) ? 'page' : undefined}
                >

                  {item.icon}
                  {' '}
                  {item.name}
                </a>
              </div>

            ))}
          </div>
          <div className="flex mt-10 justify-center items-center">
            <button type="button" onClick={logOut} className="flex text-white">
              <LogoutIcon className="h-8 w-8 mr-2" />
              <p className="text-lg">Logout</p>
            </button>
          </div>
        </div>
      </Transition>
      <div className="h-screen sticky left-0 top-0 md:block">
        <button type="button" onClick={openBar}>
          <ChevronDoubleLeftIcon className={classNames(
            open ? 'transform rotate-180 ' : '',
            'h-8 w-8 bg-gray-300 rounded text-black',
          )}
          />
        </button>
      </div>
    </>
  );
}
