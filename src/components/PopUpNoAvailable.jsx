import Image from 'next/image';
import Link from 'next/link';

export default function PopUpNoAvailable(props) {
  return (
    <div className="text-center mx-auto 0 bg-white">
      <div>
        <Image src="/images/not-found.png" alt="success" height={250} width={250} />
      </div>
      <div className="md:my-6 my-8">
        <h3 className="md:text-3xl text-lg font-medium">Campaign penelitian tidak ditemukan</h3>
        <div className="hidden sm:block">
            <p className="mt-12 mb-4 text-base md:text-xl">Buat campaign untuk penelitianmu dan dapatkan</p>
            <p className="md:text-xl text-base">pendanaan secepatnya!</p>
        </div>
        <p className="mt-8 mb-4 sm:hidden text-base md:text-xl">Buat campaign untuk penelitianmu dan dapatkan pendanaan secepatnya!</p>
      </div>
      <div>
        <Link href={props.page}>
          <a>
            <button type="button" className="hover:bg-green-750 bg-green-650 px-3 text-white w-11/12 md:w-1/2 py-3 rounded-lg"><p className="md:text-xl text-sm font-semibold md:font-bold">Buat Campaign</p></button>
          </a>
        </Link>
      </div>
    </div>
  );
}
