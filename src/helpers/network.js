import axios from 'axios';

export const BASE_URL = 'https://enigmatic-wave-05196.herokuapp.com';
// https://monke-d-return.herokuapp.com
// https://enigmatic-wave-05196.herokuapp.com
export const callAPI = async (config) => {
  try {
    Object.assign(config, {
      baseURL: BASE_URL,
    });
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return Promise.reject(error);
  }
};
