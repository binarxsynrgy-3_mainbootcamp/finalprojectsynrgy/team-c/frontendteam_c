import MahasiswaLayout from '../../../../layouts/mahasiswa/index';
import CreateCampaignContainer from '@/containers/mahasiswa/CreateCampaignContainer';

export default function CreateCampaignPage() {
  return (
    <MahasiswaLayout page="Campaign">
      <CreateCampaignContainer />
    </MahasiswaLayout>
  );
}

CreateCampaignPage.auth = true;
