import MahasiswaLayout from '../../../layouts/mahasiswa/index';
import ListCampaignContainer from '@/containers/mahasiswa/ListCampaignContainer';

export default function CampaignPage() {
  return (
    <MahasiswaLayout page="Campaign">
      <ListCampaignContainer />
    </MahasiswaLayout>
  );
}

CampaignPage.auth = true;
