import MahasiswaLayout from '../../../layouts/mahasiswa/index';
import ListPaymentContainer from '@/containers/mahasiswa/ListPaymentContainer';

export default function PaymentPage() {
  return (
    <MahasiswaLayout page="Dana">
      <ListPaymentContainer />
    </MahasiswaLayout>
  );
}

PaymentPage.auth = true;
