import MahasiswaLayout from '../../../../layouts/mahasiswa';
import DetailPaymentContainer from '@/containers/mahasiswa/DetailPaymentContainer';

export default function DetailPayment() {
  return (
    <MahasiswaLayout page="Dana">
      <DetailPaymentContainer />
    </MahasiswaLayout>
  );
}

DetailPayment.auth = true;

