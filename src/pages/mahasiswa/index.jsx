import MahasiswaLayout from '../../layouts/mahasiswa/index';
import ListOptionContainer from '@/containers/mahasiswa/ListOptionContainer';

export default function OptionPage() {
  return (
    <MahasiswaLayout page="">
      <ListOptionContainer />
    </MahasiswaLayout>
  );
}

OptionPage.auth = true;
