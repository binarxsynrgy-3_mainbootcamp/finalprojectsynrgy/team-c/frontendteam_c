import { DotsHorizontalIcon } from '@heroicons/react/solid';

export default function LoadingPage() {
  return (
    <div className="flex min-h-screen items-center justify-center w-screen">
      <DotsHorizontalIcon className="h-12 w-12 animate-ping mr-5" />
      <p className="text-2xl font-bold">Harap Menunggu</p>
    </div>
  );
}
LoadingPage.auth = true;
