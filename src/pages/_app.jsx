import Head from 'next/head';
import '../styles/globals.css';
import {useEffect} from 'react';
import { useRouter } from 'next/router';
import UserRepository from '../libraries/repositories/user';

function MyApp({ Component, pageProps}) {
  return (
    <>
      <Head>
        <meta name="description" content="Admin toBantoo" />
        <link rel="icon" href="/favicon/favicon-32x32.png" />
        <link rel="icon" href="/favicon/favicon-16x16.png" />
        <link rel="icon" href="/favicon/apple-touch-icon.png" />
        <link rel="icon" href="/favicon/android-chrome-192x192.png" />
        <link rel="icon" href="/favicon/favicon-32x32.png" />
      </Head>
      {Component.auth ? (
        <Auth>
          <Component {...pageProps} />
        </Auth>
      ) : (
        <Component {...pageProps} />
      )}
    </>
  );
}

function Auth({ children }) {
  const router = useRouter();
  const loadUser = async () => {
    try {
      const isUser = localStorage.getItem('user');
      const result = await UserRepository.auth(isUser);
      const path = router.pathname.split('/').join('/').replace(/superadmin|mahasiswa/gi, '');
      switch (result.roles[0].name) {
        case "ROLE_ADMIN":
          if (router.pathname === `/mahasiswa${path.replace('/', '')}`) return children;
          router.push('/mahasiswa');
          break;
        case "ROLE_SUPERUSER":
          if (router.pathname === `/superadmin${path.replace('/', '')}`) return children;
          router.push('/superadmin');
          break;
        default:
          router.push('/login');
      }
    } catch (error) {
      router.push('/login');
    }
  };

  useEffect(() => {
    loadUser();
  }, []);
  return children;
}

export default MyApp;
