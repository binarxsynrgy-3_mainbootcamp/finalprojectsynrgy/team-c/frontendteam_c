import Link from 'next/link';

export default function HomePage() {
  return (
    <div>
      <Link href="/login">
        <a>Login</a>
      </Link>
    </div>
  );
}
