import Link from 'next/link';
import SuperAdminLayout from '../../layouts/superadmin';
import DashboardContainer from '@/containers/superadmin/DashboardContainer';

export default function SuperAdminPage() {
  return (
    <SuperAdminLayout page="Overview">
      <DashboardContainer />
    </SuperAdminLayout>
  );
}

SuperAdminPage.auth = true;
