import SuperAdminLayout from '../../../../layouts/superadmin';
import DetailPaymentContainer from '@/containers/superadmin/DetailPaymentContainer';

export default function PaymentDetail() {
  return (
    <SuperAdminLayout page="Transaction">
      <DetailPaymentContainer />
    </SuperAdminLayout>
  );
}
