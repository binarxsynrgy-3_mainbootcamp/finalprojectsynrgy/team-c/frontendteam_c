import SuperAdminLayout from '../../../layouts/superadmin';
import ListPaymentContainer from '@/containers/superadmin/ListPaymentContainer';

export default function CampaignPage() {
  return (
    <SuperAdminLayout page="Transaction">
      <ListPaymentContainer />
    </SuperAdminLayout>
  );
}


// import Link from 'next/link';
// import { CheckIcon, XIcon } from '@heroicons/react/solid';
// import SuperAdminLayout from '../../../layouts/superadmin/superadmin';

// export default function PaymentPage() {
//   return (
//     <SuperAdminLayout page="Transaction">
//     <div className="">
//       <table className="table-fixed">
//         <thead className="h-12 bg-gray-400">
//           <tr>
//             <th className="w-1/4">Judul Campaign</th>
//             <th className="w-1/4">Nama Pembuat</th>
//             <th className="w-1/4">Amount</th>
//             <th className="w-1/4">Aksi</th>
//           </tr>
//         </thead>
//         <tbody>
//         <tr>
//           <td className="p-2">Bagaimana hubungan antara migrasi Penguin dengan hidup manusia</td>
//           <td className="p-2">Mr. Penguin Researcher</td>
//           <td className="p-2">Rp 5.000.000</td>
//           <td className="p-2 text-center text-white">
//               <div className="flex gap-5">
//               <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 py-1 rounded px-3 mx-auto">
//               <Link href={``}>
//                 <a target="_blank" className="flex items-center">
//                   <CheckIcon className="h-5 mr-2" />
//                   {' '}
//                   Accepted
//                 </a>
//               </Link>
//             </button>
//             <button type="button" className="mx-auto hover:bg-green-750 bg-red-600 py-1 rounded px-3 mx-auto">
//               <Link href={``}>
//                 <a target="_blank" className="flex items-center">
//                   <XIcon className="h-5 mr-2" />
//                   {' '}
//                   Rejected
//                 </a>
//               </Link>
//             </button>
//             </div>
//           </td>
//         </tr>
//         </tbody>
//       </table>
//     </div>
//     </SuperAdminLayout>
//   );
// }
