import SuperAdminLayout from '../../../../layouts/superadmin';
import DetailCampaignContainer from '@/containers/superadmin/DetailCampaignContainer';

export default function CampaignDetail() {
  return (
    <SuperAdminLayout page="Campaign">
      <DetailCampaignContainer />
    </SuperAdminLayout>
  );
}
