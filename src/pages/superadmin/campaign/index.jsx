import SuperAdminLayout from '../../../layouts/superadmin';
import ListCampaignContainer from '@/containers/superadmin/ListCampaignContainer';

export default function CampaignPage() {
  return (
    <SuperAdminLayout page="Campaign">
      <ListCampaignContainer />
    </SuperAdminLayout>
  );
}
