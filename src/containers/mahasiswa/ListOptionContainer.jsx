import { useRouter } from 'next/router';
import { PlusCircleIcon, CashIcon } from '@heroicons/react/solid';
import Link from 'next/link';

export default function ListOptionContainer() {
  const router = useRouter();
  return (
    <div className="mt-8 md:mt-10">
      <div className="mb-10">
        <p className="text-green-650 text-center font-semibold text-2xl">Apa yang anda butuhkan?</p>
      </div>
      <div className="md:m-4 grid grid-cols-2 pb-10 md:gap-32 gap-10">
        <div className="md:col-span-1 col-span-2">
          <div className="mx-auto h-72 md:h-96 bg-green-650 text-center hover:bg-green-750 rounded-2xl md:mr-0 md:ml-auto w-2/3 md:w-1/2 text-white">
            <Link href="mahasiswa/campaign">
              <a className="h-full w-full flex flex-col justify-center">
                <PlusCircleIcon className="mx-auto w-24 h-24 md:w-32 md:h-32" />
                <p className="font-semibold text-xl md:text-2xl mt-6">Buat Campaign</p>
              </a>
            </Link>
          </div>
        </div>
        <div className="md:col-span-1 col-span-2 md:mb-0 mb-8">
          <div className="mx-auto h-72 md:h-96 bg-green-650 text-center hover:bg-green-750 rounded-2xl md:ml-0 md:mr-auto w-2/3 md:w-1/2 text-white">
            <Link href="mahasiswa/payment">
              <a className="h-full w-full flex flex-col justify-center">
                <CashIcon className="mx-auto w-24 h-24 md:w-32 md:h-32" />
                <p className="font-semibold text-xl md:text-2xl mt-6">Ambil Dana</p>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
