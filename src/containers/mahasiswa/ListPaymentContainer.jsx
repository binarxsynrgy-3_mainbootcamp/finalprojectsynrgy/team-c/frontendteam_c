/* eslint-disable no-nested-ternary */
import { Dialog, Transition } from '@headlessui/react';
import { useState, useEffect, Fragment } from 'react';
import { InboxInIcon, XIcon } from '@heroicons/react/solid';
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ReactPaginate from 'react-paginate';
import PaymentRepository from '../../libraries/repositories/admin/payment';
import PopUpNoAvailable from '@/components/PopUpNoAvailable';
import PopUpSuccess from '@/components/PopUpSuccess';
import currency from '@/lib/currency';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function ListPaymentContainer() {
  const router = useRouter();
  const [payment, setPayment] = useState(false);
  const [selected, setSelected] = useState(0);
  const [open, setOpen] = useState(false);
  const { popUp } = router.query;
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);

  const modal = (campaign) => (
    <Transition appear show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed sm:hidden inset-0 z-10 overflow-y-auto"
        onClose={() => setOpen(!open)}
      >
        <div className="min-h-screen px-4 text-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-40"
            leave="ease-in duration-200"
            leaveFrom="opacity-40"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black opacity-50" />
          </Transition.Child>
          <span
            className="inline-block h-screen align-top"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left bg-white align-middle transition-all transform rounded-2xl">
              <div className="text-right">
                <button type="button" className="text-right border-0 outline-none" onClick={() => setOpen(!open)}><XIcon className="h-7 w-7 text-gray-500" /></button>
              </div>
              <table className="table-fixed w-full my-5">
                <tbody>
                  <tr className="bg-green-150 text-xs">
                    <td className="p-2 w-1/4">Judul Campaign:</td>
                    <td className="p-2 w-1/2">{campaign[0].projectName}</td>
                  </tr>
                  <tr className="text-xs">
                    <td className="p-2">Donasi Terkumpul:</td>
                    <td className="p-2">{currency(campaign[0].donateAll)}</td>
                  </tr>
                  <tr className="bg-green-150 text-xs">
                    <td className="p-2">Status Campaign:</td>
                    <td className="p-2">{campaign[0].duration < 0 ? 'Berakhir' : campaign[0].duration === 1 ? 'Tersisa 1 hari lagi' : 'Berjalan' }</td>
                  </tr>
                  <tr className="text-xs">
                    <td className="p-2">Dana:</td>
                    <td className="p-2">
                      {campaign[0].statusDonate === 0 ? 'Belum Diambil'
                        : campaign[0].statusDonate === 1 ? 'Pending'
                          : campaign[0].statusDonate === 2 ? 'Sudah Diambil'
                            : 'Ditolak' }
                    </td>
                  </tr>
                  <tr className="bg-green-150 text-xs">
                    <td className="p-2">Aksi:</td>
                    <td className="p-2 text-white">
                      <button
                        type="button"
                        className={classNames(
                          (campaign[0].duration < 0 && campaign[0].statusDonate === 0 && campaign[0].donateAll > 0) ? 'hover:bg-green-750 bg-green-650' : 'bg-gray-300',
                          'mx-auto py-1 rounded px-3 mx-auto',
                        )}
                      >
                        {(campaign[0].duration < 0 && campaign[0].statusDonate === 0 && campaign[0].donateAll > 0)
                          ? (
                            <Link href={`payment/detail/${campaign[0].id}`}>
                              <a target="_self" className="flex items-center">
                                <InboxInIcon className="h-5 mr-2" />
                                {' '}
                                Ambil Dana
                              </a>
                            </Link>
                          )
                          : (
                            <span className="flex items-center cursor-not-allowed text-white">
                              <InboxInIcon className="h-5 mr-2" />
                              {' '}
                              Ambil Dana
                            </span>
                          )}
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );

  const loadPayment = async () => {
    try {
      const token = localStorage.getItem('user');

      const payments = await PaymentRepository.fetchPayment(token);
      const temp = payments.filter((payment) => payment.status === true);
      const itemPayment = [];

      setPageCount(Math.ceil(temp.length / 6));

      for (let i = (currentPage * 6); i < temp.length; i++) {
        if (i === (currentPage + 1) * 6) break;
        itemPayment[i] = temp[i];
      }

      setPayment(itemPayment);
      setSelected(temp[0].id);
    } catch (error) {
      router.replace('/login');
    }
  };
  useEffect(() => {
    loadPayment();
  }, [pageCount, currentPage]);

  const handlePageChange = async (selectedPage) => {
    setCurrentPage(selectedPage.selected);
  };

  return (
    <>
      <PopUpSuccess check={popUp} title="Permintaan pengambilan dana sudah dibuat" content="Tim ToBantoo akan memproses permintaan kamu" pic="/images/payment.png" />
      <div className="mx-2 md:mx-24">
        {payment ? payment.length !== 0
          ? (
            <div>
              <div>
                <h1 className="text-2xl font-medium text-green-750 my-8">Ambil Dana</h1>
              </div>
              {/* desktop */}
              <table className="sm:table hidden table-fixed sm:w-full">
                <thead className="h-12 text-white text-sm md:text-lg bg-green-750">
                  <tr>
                    <th className="w-1/4">Judul Campaign</th>
                    <th className="w-1/5">Dana Terkumpul</th>
                    <th className="w-1/5">Status Campaign</th>
                    <th className="w-1/5">Dana</th>
                    <th className="w-1/6">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  {payment.map((obj, position) => (
                    <tr
                      key={obj.id}
                      className={classNames(
                        ((position + 1) % 2 === 0)
                          ? 'bg-green-150'
                          : '',
                      )}
                    >
                      <td className="p-2 text-xs md:text-base">{obj.projectName}</td>
                      <td className="p-2 text-center text-xs md:text-base">{currency(obj.donateAll)}</td>
                      <td className="p-2 text-center text-xs md:text-base">{obj.duration < 0 ? 'Berakhir' : obj.duration === 1 ? 'Tersisa 1 hari lagi' : 'Berjalan' }</td>
                      <td className="p-2 text-center text-xs md:text-base">
                        {obj.statusDonate === 0 ? 'Belum Diambil'
                          : obj.statusDonate === 1 ? 'Pending'
                            : obj.statusDonate === 2 ? 'Sudah Diambil'
                              : 'Ditolak' }
                      </td>
                      <td className="p-2 text-center text-xs md:text-base text-white">
                        <button
                          type="button"
                          className={classNames(
                            (obj.duration < 0 && obj.statusDonate === 0 && obj.donateAll > 0) ? 'hover:bg-green-750 bg-green-650' : 'bg-gray-300',
                            'mx-auto py-1 rounded px-3 mx-auto',
                          )}
                        >
                          {(obj.duration < 0 && obj.statusDonate === 0 && obj.donateAll > 0)
                            ? (
                              <Link href={`payment/detail/${obj.id}`}>
                                <a target="_self" className="flex items-center">
                                  <InboxInIcon className="h-8 md:h-5 mr-2" />
                                  {' '}
                                  Ambil Dana
                                </a>
                              </Link>
                            )
                            : (
                              <span className="flex items-center cursor-not-allowed">
                                <InboxInIcon className="h-8 md:h-5 mr-2" />
                                {' '}
                                Ambil Dana
                              </span>
                            )}
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              {/* desktop */}
              {/* mobile */}
              <div className="sm:hidden">
                {payment.map((obj, position) => (
                  <div
                    key={obj.id}
                    onClick={() => {
                      setOpen(!open);
                      setSelected(obj.id);
                    }}
                    className={classNames(
                      ((position + 1) % 2 === 0)
                        ? ''
                        : 'bg-green-150',
                      'px-2 py-4 font-semibold',
                    )}
                  >
                    {obj.projectName}
                    <div
                      className={classNames(
                        obj.statusDonate === 2
                          ? 'bg-green-750'
                          : 'bg-gray-300',
                        'rounded-full font-medium ml-auto mt-3 py-1 px-3 text-xs text-white w-max',
                      )}
                    >
                      {obj.statusDonate === 2 ? 'Sudah Diambil' : 'Belum Diambil' }
                    </div>
                  </div>
                ))}
                {payment.filter((obj) => obj.id === selected).length === 0 ? '' : modal(payment.filter((obj) => obj.id === selected))}
              </div>
              <div>
                <p className="italic mt-6 font-semibold">*Anda dapat mengajukan pengambilan dana jika masa campaign Anda telah berakhir</p>
              </div>
              {/* mobile */}
              <div className="my-8">
                <ReactPaginate
                  containerClassName="flex mx-auto sm:mr-0 border-2 border-gray-200 text-green-750 w-max"
                  previousLabel={<ChevronLeftIcon className="sm:h-5 mr-4 h-12" />}
                  nextLabel={<ChevronRightIcon className="sm:h-5 ml-4 h-12" />}
                  previousClassName="my-auto mx-2"
                  nextClassName="my-auto mx-2"
                  activeClassName="bg-green-750 text-base text-white"
                  breakClassName="py-2 my-auto px-4"
                  pageClassName="bg-white px-1 py-2 my-auto px-4 border-l-2 border-r-2 border-gray-200"
                  initialPage={0}
                  pageCount={pageCount}
                  marginPagesDisplayed={1}
                  onPageChange={(selectedItem) => handlePageChange(selectedItem)}
                />
              </div>
            </div>
          )
          : (
            <PopUpNoAvailable page="/mahasiswa/campaign/post" />
          ) : (
            <div className="text-2xl font-bold mt-28 text-center">
              Menampilkan...
            </div>
        )}
      </div>
    </>
  );
}
