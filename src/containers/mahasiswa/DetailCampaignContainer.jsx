import { EyeIcon, ChevronLeftIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import CampaignRepository from '../../libraries/repositories/admin/campaign';

export default function DetailCampaignContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadCampaign = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const campaign = await CampaignRepository.fetchDetailCampaign(token, id);
        setValue(campaign);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadCampaign();
  }, [id]);

  return (
    <div className="mx-2 md:mx-24 mt-8">
      <div className="cursor-pointer" onClick={() => router.replace("/mahasiswa/campaign")}>
        <h2 className="pointer-events-none text-blue-650 font-semibold flex text-2xl items-center"><ChevronLeftIcon className="w-7 h-7" />Kembali ke Campaign Request</h2>
      </div>
      <table className="table-fixed w-full my-10">
        <tbody>
          <tr className="bg-green-150">
            <td className="p-2 w-1/2">Judul Campaign</td>
            <td className="p-2 w-1/2">{!value ? 'Menampilkan...' : value.projectName}</td>
          </tr>
          <tr>
            <td className="p-2">Nama Pembuat</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.user.username}</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2">Tanggal Request</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.created_date.substring(0, 10)}</td>
          </tr>
          <tr>
            <td className="p-2">Total Budget</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.budget}</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2">Proposal Budget</td>
            <td className="p-2">
              <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 text-white py-1 rounded px-3 mx-auto">
                <Link href={value.projectDetails ? value.projectDetails : '/unknown'}>
                  <a target="_blank" className="flex items-center">
                    <EyeIcon className="h-5 mr-2" />
                    {' '}
                    Preview
                  </a>
                </Link>
              </button>
            </td>
          </tr>
          <tr>
            <td className="p-2">Surat Pengantar</td>
            <td className="p-2">
              <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 text-white py-1 rounded px-3 mx-auto">
                <Link href={value.coverLetter ? value.coverLetter : '/unknown'}>
                  <a target="_blank" className="flex items-center">
                    <EyeIcon className="h-5 mr-2" />
                    {' '}
                    Preview
                  </a>
                </Link>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
