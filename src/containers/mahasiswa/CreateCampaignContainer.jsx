import { useState } from 'react';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import { Tab } from '@headlessui/react';
import { useRouter } from 'next/router';
import CampaignRepository from '../../libraries/repositories/admin/campaign';
import UserRepository from '../../libraries/repositories/user';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const categories = [
  {name: 'Biologi'},
  {name: 'Ekonomi'},
  {name: 'Ekologi'},
  {name: 'Medis'},
  {name: 'Ilmu Komputer'},
  {name: 'Edukasi'},
  {name: 'Fisika'},
  {name: 'Teknik'},
  {name: 'Psikologi'},
  {name: 'Seni'},
  {name: 'Ilmu Sosial'},
];

const initialValues = {
  projectName: '',
  institution: '',
  profilePeneliti: '',
  projectSummary: '',
  duration: '14',
  projectImg: '',
  budget: '',
  budgetDetails: '',
  projectBackground: '',
  projectUrgency: '',
  projectGoal: '',
  researchMethods: '',
  projectDetails: '',
  coverLetter: '',
  user: {
  },
};

export default function CreateCampaignPage() {
  // Button kategori
  const [category, setCategory] = useState('Biologi');
  const router = useRouter();
  // Button kategori
  const [loading, setLoading] = useState(false);

  const [checkA, setCheckA] = useState(false);
  const [checkB, setCheckB] = useState(false);
  const handleOnSubmit = async (values, {resetForm}) => {
    try {
      setLoading(true);
      const token = localStorage.getItem('user');
      const user = await UserRepository.auth(token);
      const fileProjectImg = await CampaignRepository.postFile(token, values.projectImg);
      const fileProjectDetails = await CampaignRepository.postFile(token, values.projectDetails);
      const fileCoverLetter = await CampaignRepository.postFile(token, values.coverLetter);

      values.projectImg = fileProjectImg.fileDownloadUri;
      values.projectDetails = fileProjectDetails.fileDownloadUri;
      values.coverLetter = fileCoverLetter.fileDownloadUri;
      values.user.id = user.id;
      values.duration = parseInt(values.duration);
      values.category = category;

      const response = await CampaignRepository.requestCampaign(token, values);
      setTimeout(() => {
        setLoading(false);
        if (response) {
          router.push(
            {
              pathname: '/mahasiswa/campaign',
              query: { popUp: 'true' },
            },
            '/mahasiswa/campaign',
          );
        }
      }, 1000);
    } catch (error) {
      resetForm(initialValues);
    }
  };
  return (
    <div className="mb-10 md:mb-0 mx-2 md:mx-10">
      <div>
        <h1 className="text-2xl font-medium text-green-650 mt-8 mb-10">Buat Campaign</h1>
      </div>
      <Tab.Group>
        {/* Header */}
        <Tab.List className="gap-2 md:gap-0 mt-10 text-lg md:rounded-lg grid md:grid-cols-6 font-bold mb-6">
          <Tab
            key="Dasar"
            className={({ selected }) => classNames(
              'py-2 px-8 rounded md:rounded-l-lg hover:bg-green-750 hover:border-2 hover:border-green-750 hover:text-white',
              selected
                ? 'bg-green-650 text-white border border-1 border-green-750'
                : 'bg-gray-100 text-gray-400 border border-1 border-gray-300',
            )}
          >
            Dasar
          </Tab>
          <Tab
            key="Anggaran"
            className={({ selected }) => classNames(
              'py-2 px-8 rounded hover:bg-green-750 hover:border-2 hover:border-green-750  hover:text-white',
              selected
                ? 'bg-green-650 text-white border border-1 border-green-750'
                : 'bg-gray-100 text-gray-400 border border-1 border-gray-300',
            )}
          >
            Anggaran
          </Tab>
          <Tab
            key="Penelitian"
            className={({ selected }) => classNames(
              'py-2 px-8 rounded hover:bg-green-750 hover:border-2 hover:border-green-750  hover:text-white',
              selected
                ? 'bg-green-650 text-white border border-1 border-green-750'
                : 'bg-gray-100 text-gray-400 border border-1 border-gray-300',
            )}
          >
            Penelitian
          </Tab>
          <Tab
            key="Lampiran"
            className={({ selected }) => classNames(
              'py-2 px-8 rounded md:rounded-r-lg hover:bg-green-750 hover:border-2 hover:border-green-750  hover:text-white',
              selected
                ? 'bg-green-650 text-white border border-1 border-green-750'
                : 'bg-gray-100 text-gray-400 border border-1 border-gray-300',
            )}
          >
            Lampiran
          </Tab>
          <Tab
            key="Buat"
            className={({ selected }) => classNames(
              'md:ml-auto rounded md:col-span-2 md:px-16 py-2 md:rounded-lg hover:border-2 hover:border-green-750  hover:bg-green-750 hover:text-white',
              selected
                ? 'bg-green-650 text-white border border-1 border-green-750'
                : 'bg-gray-100 text-gray-400 border border-1 border-gray-300',
            )}
          >
            Buat
          </Tab>
        </Tab.List>
        {/* Header */}
        <Tab.Panels>
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
              setFieldValue,
            }) => (
              <form className="relative md:p-10" onSubmit={handleSubmit}>
                {/* Dasar */}
                <Tab.Panel>
                  <label className="flex flex-col" htmlFor="projectName">
                    <span className="text-xl font-medium mb-2 text-green-650">Judul</span>
                    <Field
                      required
                      name="projectName"
                      id="projectName"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="Tulis judul campaign-mu"
                    />
                  </label>
                  <h5 className="text-xl font-medium mb-2 mt-5 text-green-650">Kategori</h5>
                  <div className="lg:w-2/5 my-5">
                    {/* Button kategori */}
                    {categories.map((obj, position) => (
                      <button
                        key={position}
                        onClick={() => setCategory(obj.name)}
                        type="button"
                        className={classNames(
                          'text-base mx-1 my-2 font-medium py-2 px-8 rounded-lg hover:bg-green-750 hover:text-white',
                          category === obj.name
                            ? 'bg-green-650 text-white border border-1 border-green-750'
                            : 'bg-gray-100 text-gray-400 border border-1 border-gray-300',
                        )}
                      >
                        {obj.name}
                      </button>
                    ))}
                    {/* Button kategori */}

                  </div>
                  <label className="flex flex-col" htmlFor="duration">
                    <span className="text-xl font-medium mb-2 text-green-650">Durasi</span>
                    <Field
                      required
                      name="duration"
                      id="duration"
                      component="select"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="Durasi Campaign"
                    >
                      <option value="14">14 Hari</option>
                      <option value="30">30 Hari</option>
                      <option value="60">60 Hari</option>
                      <option value="90">90 Hari</option>
                    </Field>
                  </label>
                  <label className="flex flex-col mt-8" htmlFor="profilePeneliti">
                    <span className="text-xl font-medium mb-2 text-green-650">Nama Penenliti</span>
                    <Field
                      required
                      name="profilePeneliti"
                      id="profilePeneliti"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="Tulis nama peneliti"
                    />
                  </label>
                  <label className="flex flex-col mt-8" htmlFor="institution">
                    <span className="text-xl font-medium mb-2 text-green-650">Institusi</span>
                    <Field
                      required
                      name="institution"
                      id="institution"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="Tulis nama insitusimu"
                    />
                  </label>
                  <label className="flex flex-col mt-8" htmlFor="projectSummary">
                    <span className="text-xl font-medium mb-2 text-green-650">Rangkuman Campaign</span>
                    <Field
                      required
                      name="projectSummary"
                      id="projectSummary"
                      as="textarea"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black h-32 focus:outline-none"
                      placeholder="Tulis secara singkat tentang campaignmu"
                    />
                  </label>
                  <label className="flex flex-col mt-8" htmlFor="projectImg">
                    <span className="text-xl font-medium mb-2 text-green-650">Foto Campaign</span>
                    <input
                      required
                      name="projectImg"
                      id="projectImg"
                      type="file"
                      accept="image/*"
                      onChange={(e) => {
                        const file = e.target.files;
                        setFieldValue('projectImg', file[0]);
                      }}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                    />
                  </label>
                  <p className="italic">Maksimal file upload 1 Mb</p>
                </Tab.Panel>
                {/* Dasar */}
                {/* Anggaran */}
                <Tab.Panel>
                  <label className="flex flex-col" htmlFor="budget">
                    <span className="text-xl font-medium mb-2 text-green-650">Anggaran</span>
                    <Field
                      required
                      name="budget"
                      id="budget"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="Tulis total anggaran yang dibutuhkan"
                    />
                  </label>
                  <div className="text-green-650 italic">
                    <p>*5% dari total donasi yang terkumpul akan dipotong untuk biaya platform</p>
                    <p>**Donasi dapat diambil setelah campaign berakhir</p>
                  </div>
                  <label className="flex flex-col mt-8" htmlFor="budgetDetails">
                    <span className="text-xl font-medium mb-2 text-green-650">Detail Anggaran</span>
                    <Field
                      required
                      name="budgetDetails"
                      id="budgetDetails"
                      as="textarea"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black h-32 focus:outline-none"
                      placeholder="Tulis anggaran secara lebih spesifik"
                    />
                  </label>
                </Tab.Panel>
                {/* Anggaran */}
                {/* Penelitian */}
                <Tab.Panel>
                  <label className="flex flex-col" htmlFor="projectBackground">
                    <span className="text-xl font-medium mb-2 text-green-650">Latar Belakang</span>
                    <Field
                      required
                      name="projectBackground"
                      id="projectBackground"
                      as="textarea"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black h-32 focus:outline-none"
                      placeholder="Tulis tentang latar belakang penelitian yang akan dilakukan"
                    />
                  </label>
                  <label className="flex flex-col mt-8" htmlFor="projectUrgency">
                    <span className="text-xl font-medium mb-2 text-green-650">Urgensi Penelitian</span>
                    <Field
                      required
                      name="projectUrgency"
                      id="projectUrgency"
                      as="textarea"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black h-32 focus:outline-none"
                      placeholder="Tulis kenapa penelitian ini penting"
                    />
                  </label>

                  <label className="flex flex-col mt-8" htmlFor="projectGoal">
                    <span className="text-xl font-medium mb-2 text-green-650">Goal Penelitian</span>
                    <Field
                      required
                      name="projectGoal"
                      id="projectGoal"
                      as="textarea"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black h-32 focus:outline-none"
                      placeholder="Tulis apa yang menjadi tujuan penelitian ini"
                    />
                  </label>
                  <label className="flex flex-col mt-8" htmlFor="researchMethods">
                    <span className="text-xl font-medium mb-2 text-green-650">Metode Penelitian</span>
                    <Field
                      required
                      name="researchMethods"
                      id="researchMethods"
                      as="textarea"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black h-32 focus:outline-none"
                      placeholder="Tulis metode apa yang akan digunakan"
                    />
                  </label>
                </Tab.Panel>
                {/* Penelitian */}
                {/* Lampiran */}
                <Tab.Panel>
                  {/* <label className="flex flex-col" htmlFor="projectDetails">
                    <span className="text-xl font-medium mb-2 text-green-650">File Proposal Penelitian</span>
                    <Field
                      required
                      name="projectDetails"
                      id="projectDetails"
                      type="file"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="File Proposal"
                        />
                  </label> */}
                  <label className="flex flex-col mt-8" htmlFor="projectDetails">
                    <span className="text-xl font-medium mb-2 text-green-650">File Proposal Penelitian</span>
                    <input
                      required
                      name="projectDetails"
                      id="projectDetails"
                      type="file"
                      onChange={(e) => {
                        const file = e.target.files;
                        setFieldValue('projectDetails', file[0]);
                      }}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                    />
                  </label>
                  <p className="italic">Maksimal file upload 1 Mb</p>
                  <label className="flex flex-col mt-8" htmlFor="coverLetter">
                    <span className="text-xl font-medium mb-2 text-green-650">Surat Pengantar dari Institusi</span>
                    <input
                      required
                      name="coverLetter"
                      id="coverLetter"
                      type="file"
                      onChange={(e) => {
                        const file = e.target.files;
                        setFieldValue('coverLetter', file[0]);
                      }}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                    />
                  </label>
                  <p className="italic">Maksimal file upload 1 Mb</p>
                  {/* <label className="flex flex-col mt-8" htmlFor="coverLetter">
                    <span className="text-xl font-medium mb-2 text-green-650">Surat Pengantar</span>
                    <input
                      type="file"
                      name="coverLetter"
                      id="coverLetter"
                      className="hidden"
                    />
                    <Field
                      required
                      name="coverLetter"
                      id="coverLetter"
                      type="file"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                      placeholder="Surat Pengantar"
                    />
                  </label> */}
                </Tab.Panel>
                {/* Lampiran */}
                {/* Buat */}
                <Tab.Panel>
                  <span className="text-xl font-medium mb-2 text-green-650">Persetujuan</span>
                  <label className="flex items-center flex-row mt-8">
                    <input
                      type="checkbox"
                      onChange={() => setCheckA(!checkA)}
                      checked={checkA}
                      className="form-checkbox h-6 w-6 checked:ring-offset-0 checked:ring-0 focus:ring-0 text-green-650"
                    />
                    <span className="ml-5 w-full">Dokumen yang saya unggah sesuai dengan syarat dan ketentuan dan dapat dipertanggungjawabkan kebenarannya</span>
                  </label>
                  <label className="flex items-center flex-row mt-8">
                    <input
                      type="checkbox"
                      onChange={() => setCheckB(!checkB)}
                      checked={checkB}
                      className="form-checkbox h-6 w-6 checked:ring-offset-0 checked:ring-0 focus:ring-0 text-green-650"
                    />
                    <span className="ml-5 w-full">Saya setuju dengan syarat dan ketentuan yang berlaku</span>
                  </label>
                  <div className="text-center mt-10">
                    <button
                      disabled={!(checkA & checkB)}
                      type="submit"
                      className={classNames(
                        checkA & checkB ? 'bg-green-650 hover:bg-green-750' : 'bg-green-150 cursor-not-allowed',
                        'py-3 w-1/2 mx-auto text-white',
                      )}
                    >
                      {loading ? 'Harap Tunggu...' : 'Buat Campaign'}
                    </button>
                  </div>
                </Tab.Panel>
              </form>
            )}
          </Formik>
          {/* Buat */}
        </Tab.Panels>
      </Tab.Group>
    </div>
  );
}
