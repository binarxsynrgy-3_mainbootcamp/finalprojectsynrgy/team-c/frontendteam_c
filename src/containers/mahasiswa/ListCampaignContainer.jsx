/* eslint-disable no-nested-ternary */
import { Dialog, Transition } from '@headlessui/react';
import { useState, useEffect, Fragment } from 'react';
import ReactPaginate from 'react-paginate';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { ChevronLeftIcon, ChevronRightIcon, XIcon } from '@heroicons/react/outline';
import CampaignRepository from '../../libraries/repositories/admin/campaign';
import PopUpSuccess from '@/components/PopUpSuccess';
import PopUpNoAvailable from '@/components/PopUpNoAvailable';
import currency from '@/lib/currency';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function ListCampaignContainer() {
  const router = useRouter();
  const [selected, setSelected] = useState(0);
  const [campaign, setCampaign] = useState(false);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const { popUp } = router.query;
  const [open, setOpen] = useState(false);

  const modal = (campaign) => (
    <Transition appear show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed sm:hidden inset-0 z-10 overflow-y-auto"
        onClose={() => setOpen(!open)}
      >
        <div className="min-h-screen px-4 text-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-40"
            leave="ease-in duration-200"
            leaveFrom="opacity-40"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black opacity-50" />
          </Transition.Child>
          <span
            className="inline-block h-screen align-top"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left bg-white align-middle transition-all transform rounded-2xl">
              <div className="text-right">
                <button type="button" className="text-right border-0 outline-none" onClick={() => setOpen(!open)}><XIcon className="h-7 w-7 text-gray-500" /></button>
              </div>
              <table className="table-fixed w-full my-5">
                <tbody>
                  <tr className="bg-green-150 text-xs">
                    <td className="p-2 w-1/4">Judul Campaign:</td>
                    <td className="p-2 w-1/2">{campaign[0].projectName}</td>
                  </tr>
                  <tr className="text-xs">
                    <td className="p-2">Donasi Terkumpul:</td>
                    <td className="p-2">{currency(campaign[0].donateAll)}</td>
                  </tr>
                  <tr className="bg-green-150 text-xs">
                    <td className="p-2">Status Approval:</td>
                    <td className="p-2">{campaign[0].status === true ? 'Diterima' : campaign[0].status === false ? 'Ditolak' : 'Pending' }</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );

  const loadCampaign = async () => {
    try {
      const token = localStorage.getItem('user');
      const campaigns = await CampaignRepository.fetchCampaign(token);
      const itemCampaign = [];

      setPageCount(Math.ceil(campaigns.length / 6));

      for (let i = (currentPage * 6); i < campaigns.length; i++) {
        if (i === (currentPage + 1) * 6) break;
        itemCampaign[i] = campaigns[i];
      }
      setCampaign(itemCampaign);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadCampaign();
  }, [pageCount, currentPage]);

  const handlePageChange = async (selectedPage) => {
    setCurrentPage(selectedPage.selected);
  };

  return (
    <>
    <PopUpSuccess check={popUp} title="Campaign berhasil dibuat" content="Tim ToBantoo akan memeriksa campaign kamu dahulu" pic="/images/success-pic.png" />
    <div className="mx-2 md:mx-24">
      {campaign
        ? campaign.length !== 0
          ? (
            <div>
              <div className="w-full justify-between mt-12 mb-6 md:mb-8 flex items-center">
                <h1 className="md:text-2xl text-lg font-medium text-green-750">Campaign Request</h1>
                <Link href="/mahasiswa/campaign/post">
                  <a>
                    <button type="button" className="bg-white text-xs md:text-base flex text-green-650 hover:bg-green-750 hover:text-white px-5 border-2 border-green-650 items-center font-semibold py-3 rounded-lg">
                      Buat Campaign
                    </button>
                  </a>
                </Link>
              </div>
              <div className="h-96 hidden sm:block">
                <table className="sm:table table-fixed sm:w-full">
                  <thead className="h-12 text-sm md:text-lg text-white bg-green-750">
                    <tr>
                      <th className="w-1/5 md:w-1/4">Judul Campaign</th>
                      <th className="w-1/4 md:w-1/5">Nama Pembuat</th>
                      <th className="w-1/5">Status Approval</th>
                    </tr>
                  </thead>
                  <tbody>
                    {campaign.map((obj, position) => (
                      <tr
                        key={obj.id}
                        className={classNames(
                          ((position + 1) % 2 === 0)
                            ? 'bg-green-150'
                            : '',
                        )}
                      >
                        <td className="p-2 text-xs md:text-base ">{obj.projectName}</td>
                        <td className="p-2 text-center align-middle text-xs md:text-base">{obj.user.username}</td>
                        <td className="p-2 text-center align-middle text-xs md:text-base">{obj.status === true ? 'Diterima' : obj.status === false ? 'Ditolak' : 'Pending' }</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
              {/* mobile */}
              <div className="sm:hidden">
                {campaign.map((obj, position) => (
                  <div
                    key={obj.id}
                    onClick={() => {
                      setOpen(!open);
                      setSelected(obj.id);
                    }}
                    className={classNames(
                      ((position + 1) % 2 === 0)
                        ? ''
                        : 'bg-green-150',
                      'px-2 py-4 font-semibold',
                    )}
                  >
                    {obj.projectName}
                    <div
                      className="bg-green-750 rounded-full font-medium ml-auto mt-3 py-1 px-3  text-xs text-white w-max"
                    >
                      {obj.status === true ? 'Berjalan' : obj.status === false ? 'Ditolak' : 'Pending' }
                    </div>

                  </div>
                ))}
                {campaign.filter((obj) => obj.id === selected).length === 0 ? '' : modal(campaign.filter((obj) => obj.id === selected))}
              </div>
              {/* mobile */}
              <div className="my-8">
                <ReactPaginate
                  containerClassName="flex mx-auto sm:mr-0 border-2 border-gray-200 text-green-750 w-max"
                  previousLabel={<ChevronLeftIcon className="sm:h-5 mr-4 h-12" />}
                  nextLabel={<ChevronRightIcon className="sm:h-5 ml-4 h-12" />}
                  previousClassName="my-auto mx-2"
                  nextClassName="my-auto mx-2"
                  activeClassName="bg-green-750 text-base text-white"
                  breakClassName="py-2 my-auto px-4"
                  pageClassName="bg-white px-1 py-2 my-auto px-4 border-l-2 border-r-2 border-gray-200"
                  initialPage={0}
                  pageCount={pageCount}
                  marginPagesDisplayed={1}
                  onPageChange={(selectedItem) => handlePageChange(selectedItem)}
                />
              </div>
            </div>
          )
          : (
            <PopUpNoAvailable page="/mahasiswa/campaign/post" />
          )
        : (
          <div className="text-2xl font-bold mt-28 text-center">
            Menampilkan...
          </div>
        )}
    </div>
    </>
  );
}
