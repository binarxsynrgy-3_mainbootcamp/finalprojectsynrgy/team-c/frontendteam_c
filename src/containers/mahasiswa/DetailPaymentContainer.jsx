import { EyeIcon, ChevronLeftIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import PaymentRepository from '../../libraries/repositories/admin/payment';
import currency from '@/lib/currency';

const initialValues = {
  norek: '',
};

const validationSchema = Yup.object({
  norek: Yup.string().required('Harap Isi Nomor Rekening Anda'),
});

export default function DetailPaymentContainer() {
  const [value, setValue] = useState(false);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadCampaign = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const campaign = await PaymentRepository.fetchDetailPayment(token, id);
        setValue(campaign);

      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadCampaign();
  }, [id]);

  const handleOnSubmit = async (values, {resetForm}) => {
    try {
      setLoading(true);
      const token = localStorage.getItem('user');
      const response = await PaymentRepository.requestPayment(token, id, values);
      setTimeout(() => {
        setLoading(false);
        if (response) {
          router.push(
            {
              pathname: '/mahasiswa/payment',
              query: { popUp: 'true' },
            },
            '/mahasiswa/payment',
          );
        }
      }, 1000);
    } catch (error) {
      resetForm(initialValues);
    }
  };

  return (
    <div className="mx-2 md:mx-24 mt-8">
      <div className="cursor-pointer" onClick={() => router.replace("/mahasiswa/payment")}>
        <h2 className="text-blue-650 font-semibold flex text-2xl items-center">
          <ChevronLeftIcon className="w-7 h-7" />
          Kembali ke Payment Request
        </h2>
      </div>
      <table className="table-fixed w-full my-10">
        <tbody>
          <tr className="bg-green-150">
            <td className="p-2 w-1/2">Judul Campaign</td>
            <td className="p-2 w-1/2">{!value ? 'Menampilkan...' : value.["Nama Campaign "]}</td>
          </tr>
          <tr>
            <td className="p-2">Status Campaign</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.["Status Campaign "] < 0 ? 'Berakhir' : 'Berjalan' }</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2">Jumlah Donasi</td>
            <td className="p-2">{!value ? 'Menampilkan...' : currency(value.["Total Donate "])}</td>
          </tr>
          <tr>
            <td className="p-2">Dana yang dapat Diambil</td>
            <td className="p-2">{!value ? 'Menampilkan...' : currency(value.["total you can get"])}</td>
          </tr>
          <tr>
            <td colSpan="2" className="h-20 text-green-650 italic">
              <p>*5% dari total donasi yang terkumpul akan dipotong untuk biaya platform</p>
              <p>**Pengambilan dana berarti mengambil semua dana yang tersedia</p>
            </td>
          </tr>
        </tbody>
      </table>
      <div>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleOnSubmit}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            touched,
            errors,
            setFieldValue,
          }) => (
            <form onSubmit={handleSubmit}>
              <label className="flex flex-col" htmlFor="norek">
                <span className="text-xl font-medium mb-2 text-green-650">Nomor Rekening</span>
                <Field
                  required
                  name="norek"
                  id="norek"
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className="p-2 rounded focus:border-none border-2 border-black focus:outline-none"
                  placeholder="Masukkan nomor rekening tujuan"
                />
              </label>
              <div className="text-center">
                <button type="submit" className="mt-12 bg-green-650 rounded-md md:py-3 py-2 w-2/3 md:w-1/2 text-white" disabled={loading}>
                  {loading ? 'Harap Tunggu...' : 'Ambil Dana'}
                </button>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
}
