/* eslint-disable no-nested-ternary */
import { useState, useEffect } from 'react';
import { Tab } from '@headlessui/react';
import { EyeIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import PaymentRepository from '../../libraries/repositories/superadmin/payment';
import currency from '@/lib/currency';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function ListPaymentContainer() {
  const [request, setRequest] = useState(false);
  const [valid, setValid] = useState(false);
  const loadPayment = async () => {
    try {
      const token = localStorage.getItem('user');
      const payments = await PaymentRepository.fetchAllPayment(token);
      const validPayment = payments.filter((payment) => payment.statusDonate === 2);
      const requestPayment = payments.filter((payment) => payment.statusDonate === 1);
      setValid(validPayment);
      setRequest(requestPayment);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadPayment();
  }, []);
  return (
    <div className="m-4">
      <div>
        <h1 className="text-2xl font-medium text-green-750">Transaction Request</h1>
      </div>
      <Tab.Group>
        <Tab.List className="mt-10 text-white text-lg bg-gray-300 w-max font-bold mb-6">
          <Tab
            key="Requested"
            className={({ selected }) => classNames(
              'py-1 px-16 rounded-lg',
              selected
                ? 'bg-green-650'
                : 'bg-gray-300',
            )}
          >
            Requested
          </Tab>
          <Tab
            key="Accepted"
            className={({ selected }) => classNames(
              'py-1 px-16 rounded-lg',
              selected
                ? 'bg-green-650'
                : 'bg-gray-300',
            )}
          >
            Accepted
          </Tab>
        </Tab.List>
        <Tab.Panels>
          {/* Requested */}
          <Tab.Panel>
            <table className="table-fixed w-full">
              <thead className="h-12 text-white bg-green-750">
                <tr>
                  <th className="w-1/4">Judul Campaign</th>
                  <th className="w-1/5">Nama Pembuat</th>
                  <th className="w-1/5">Jumlah Dana Diambil</th>
                  <th className="w-1/6">Aksi</th>
                </tr>
              </thead>
              <tbody>
                {request ? request.length !== 0 ? request.map((obj, position) => (
                  <tr
                    key={obj.id}
                    className={classNames(
                      ((position + 1) % 2 === 0)
                        ? 'bg-green-150'
                        : '',
                    )}
                  >
                    <td className="p-2">{obj.projectName}</td>
                    <td className="p-2 text-center">{obj.user.username}</td>
                    <td className="p-2 text-center">{currency(obj.donateAll)}</td>
                    <td className="p-2 text-center text-white">
                      <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 py-1 rounded px-3 mx-auto">
                        <Link href={`payment/detail/${obj.id}`}>
                          <a target="_self" className="flex items-center">
                            <EyeIcon className="h-5 mr-2" />
                            {' '}
                            Preview
                          </a>
                        </Link>
                      </button>
                    </td>
                  </tr>
                )) : (
                  <tr>
                      <td colSpan="4" className="text-2xl font-bold h-52 text-center">Belum ada request payment</td>
                    </tr>
                )
                  : (
                    <tr>
                      <td colSpan="4" className="text-2xl font-bold h-52 text-center">Menampilkan...</td>
                    </tr>
                  )}
              </tbody>
            </table>
          </Tab.Panel>
          {/* Requested */}

          {/* Accepted */}
          <Tab.Panel>
            <table className="table-fixed w-full">
              <thead className="h-12 text-white bg-green-750">
                <tr>
                  <th className="w-1/4">Judul Campaign</th>
                  <th className="w-1/5">Nama Pembuat</th>
                  <th className="w-1/5">Jumlah Dana Diambil</th>
                </tr>
              </thead>
              <tbody>
                {valid ? valid.length !== 0 ? valid.map((obj, position) => (
                  <tr
                    key={obj.id}
                    className={classNames(
                      ((position + 1) % 2 === 0)
                        ? 'bg-green-150'
                        : '',
                    )}
                  >
                    <td className="p-2">{obj.projectName}</td>
                    <td className="p-2 text-center">{obj.user.username}</td>
                    <td className="p-2 text-center">{currency(obj.donateAll)}</td>
                  </tr>
                )) : (
                  <tr>
                    <td colSpan="3" className="text-2xl font-bold h-52 text-center">Data belum tersedia</td>
                  </tr>
                )
                  : (
                    <tr>
                      <td colSpan="3" className="text-2xl font-bold h-52 text-center">Menampilkan...</td>
                    </tr>
                  )}
              </tbody>
            </table>

          </Tab.Panel>
          {/* Accepted */}

        </Tab.Panels>
      </Tab.Group>
    </div>
  );
}
