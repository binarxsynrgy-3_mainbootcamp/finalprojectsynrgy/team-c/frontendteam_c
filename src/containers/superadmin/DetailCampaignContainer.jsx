import Head from 'next/head';
import { EyeIcon, ChevronLeftIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import CampaignRepository from '../../libraries/repositories/superadmin/campaign';

export default function DetailCampaignContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadCampaign = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const campaign = await CampaignRepository.fetchDetailCampaign(token, id);
        setValue(campaign);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadCampaign();
  }, [id]);
  const accept = async () => {
    const token = localStorage.getItem('user');
    await CampaignRepository.acceptCampaign(token, id);
    router.replace('/superadmin/campaign');
  };

  const decline = async () => {
    const token = localStorage.getItem('user');
    await CampaignRepository.declineCampaign(token, id);
    router.replace('/superadmin/campaign');
  };

  return (
    <div className="m-4">
      <div className="cursor-pointer" onClick={() => router.back()}>
        <h2 className="pointer-events-none text-blue-800 font-semibold flex text-2xl items-center"><ChevronLeftIcon className="w-7 h-7" />Kembali ke Campaign Request</h2>
      </div>
      <table className="table-fixed w-full my-10">
        <tbody>
          <tr className="bg-green-150">
            <td className="p-2 w-1/2 font-bold">Judul Campaign</td>
            <td className="p-2 w-1/2">{!value ? 'Menampilkan...' : value.projectName}</td>
          </tr>
          <tr>
            <td className="p-2 font-bold">Nama Pembuat</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.user.username}</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2 font-bold">Tanggal Request</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.created_date.substring(0, 10)}</td>
          </tr>
          <tr>
            <td className="p-2 font-bold">Total Budget</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.budget}</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2 font-bold">Proposal Budget</td>
            <td className="p-2">
              <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 text-white py-1 rounded px-3 mx-auto">
                <Link href={value.projectDetails ? value.projectDetails : '/unknown'}>
                  <a target="_blank" className="flex items-center">
                    <EyeIcon className="h-5 mr-2" />
                    {' '}
                    Preview
                  </a>
                </Link>
              </button>
            </td>
          </tr>
          <tr>
            <td className="p-2 font-bold">Surat Pengantar</td>
            <td className="p-2">
              <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 text-white py-1 rounded px-3 mx-auto">
                <Link href={value.coverLetter ? value.coverLetter : '/unknown'}>
                  <a target="_blank" className="flex items-center">
                    <EyeIcon className="h-5 mr-2" />
                    {' '}
                    Preview
                  </a>
                </Link>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
      <div className="flex items-center justify-center text-white gap-20">
        <button type="button" onClick={accept} className="hover:bg-blue-200 bg-blue-400 py-3 px-20 rounded">Terima</button>
        <button type="button" onClick={decline} className="hover:bg-red-200 bg-red-400 py-3 px-20 rounded">Tolak</button>
      </div>
    </div>
  );
}
