import { useState, useEffect } from 'react';
import { Tab } from '@headlessui/react';
import { EyeIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import { useRouter } from 'next/router';
import CampaignRepository from '../../libraries/repositories/superadmin/campaign';
import PaymentRepository from '../../libraries/repositories/superadmin/payment';
import currency from '@/lib/currency';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function ListCampaignContainer() {
  const router = useRouter();
  const [campaign, setCampaign] = useState(false);
  const [payment, setPayment] = useState(false);
  const loadLatestData = async () => {
    try {
      const token = localStorage.getItem('user');
      const campaigns = await CampaignRepository.fetchAllCampaign(token);
      const payments = await PaymentRepository.fetchAllPayment(token);
      const requestCampaign = campaigns.filter((campaign) => campaign.status === null);
      const requestPayment = payments.filter((payment) => payment.statusDonate === 1);

      const arr = [];
      const temp = [];
      for (let i = requestCampaign.length - 1; i >= 0; i--) {
        arr[i] = requestCampaign[i];
        if (i === (requestCampaign.length - 5)) {
          break;
        }
      }
      setCampaign(arr);
      for (let i = requestPayment.length - 1; i >= 0; i--) {
        temp[i] = requestPayment[i];
        if (i === (requestPayment.length - 5)) {
          break;
        }
      }
      setPayment(temp);
    } catch (error) {
      router.push('/login');
    }
  };
  useEffect(() => {
    loadLatestData();
  }, [router.isReady]);
  return (
    <div className="m-4">
      {/* Campaign */}
      <div className="h-1/2">
        <div>
          <h1 className="text-2xl my-4 font-medium text-green-750">Campaign Request Terbaru</h1>
        </div>

        <table className="table-fixed w-full">
          <thead className="h-12 text-white bg-green-750">
            <tr>
              <th className="w-1/4">Judul Campaign</th>
              <th className="w-1/5">Tanggal Request</th>
              <th className="w-1/6">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {campaign ? campaign.length !== 0 ? campaign.map((obj, position) => (
              <tr
                key={obj.id}
                className={classNames(
                  ((position + 1) % 2 === 0)
                    ? 'bg-green-150'
                    : '',
                )}
              >
                <td className="p-2">{obj.projectName}</td>
                <td className="p-2 text-center">{obj.created_date.substring(0, 10)}</td>
                <td className="p-2 text-center text-white">
                  <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 py-1 rounded px-3 mx-auto">
                    <Link href={`superadmin/campaign/detail/${obj.id}`}>
                      <a target="_self" className="flex items-center">
                        <EyeIcon className="h-5 mr-2" />
                        {' '}
                        Preview
                      </a>
                    </Link>
                  </button>
                </td>
              </tr>
            ))
              : (
                <tr>
                  <td colSpan="3" className="text-2xl font-bold h-52 text-center">Belum ada request campaign</td>
                </tr>
              )
              : (
                <tr>
                  <td colSpan="3" className="text-2xl font-bold h-52 text-center">Memuat...</td>
                </tr>
              )}
          </tbody>
        </table>
      </div>
      {/* Campaign */}
      {/* Payment */}
      <div className="h-1/2">
        <div>
          <h1 className="text-2xl my-4 font-medium text-green-750">Payment Request Terbaru</h1>
        </div>
        <table className="table-fixed w-full">
          <thead className="h-12 text-white bg-green-750">
            <tr>
              <th className="w-1/4">Judul Campaign</th>
              <th className="w-1/5">Jumlah Dana Diambil</th>
              <th className="w-1/6">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {payment ? payment.length !== 0 ? payment.map((obj, position) => (
              <tr
                key={obj.id}
                className={classNames(
                  ((position + 1) % 2 === 0)
                    ? 'bg-green-150'
                    : '',
                )}
              >
                <td className="p-2">{obj.projectName}</td>
                <td className="p-2 text-center">{currency(obj.donateAll)}</td>
                <td className="p-2 text-center text-white">
                  <button type="button" className="mx-auto hover:bg-green-750 bg-green-650 py-1 rounded px-3 mx-auto">
                    <Link href={`superadmin/payment/detail/${obj.id}`}>
                      <a target="_self" className="flex items-center">
                        <EyeIcon className="h-5 mr-2" />
                        {' '}
                        Preview
                      </a>
                    </Link>
                  </button>
                </td>
              </tr>
            ))
              : (
                <tr>
                  <td colSpan="3" className="text-2xl font-bold h-52 text-center">Belum ada request payment</td>
                </tr>
              )
              : (
                <tr>
                  <td colSpan="3" className="text-2xl font-bold h-52 text-center">Memuat...</td>
                </tr>
              )}
          </tbody>
        </table>
      </div>

      {/* Payment */}
    </div>
  );
}
