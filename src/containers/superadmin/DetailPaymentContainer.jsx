import { ChevronLeftIcon } from '@heroicons/react/solid';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import PaymentRepository from '../../libraries/repositories/superadmin/payment';
import currency from '@/lib/currency';

export default function DetailPaymentContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadPayment = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const payment = await PaymentRepository.fetchDetailPayment(token, id);
        setValue(payment);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadPayment();
  }, [id]);
  const accept = async () => {
    const token = localStorage.getItem('user');
    await PaymentRepository.acceptPayment(token, id);
    router.replace('/superadmin/payment');
  };

  const decline = async () => {
    const token = localStorage.getItem('user');
    await PaymentRepository.declinePayment(token, id);
    router.replace('/superadmin/payment');
  };
  return (
    <div className="m-4">
      <div className="cursor-pointer" onClick={() => router.back()}>
        <h2 className="pointer-events-none text-blue-800 font-semibold flex text-2xl items-center">
          <ChevronLeftIcon className="w-7 h-7" />
          Kembali ke Transaction Request
        </h2>
      </div>
      <table className="table-fixed w-full my-10">
        <tbody>
          <tr className="bg-green-150">
            <td className="p-2 w-1/2 font-bold">Judul Campaign</td>
            <td className="p-2 w-1/2">{!value ? 'Menampilkan...' : value.data.projectName}</td>
          </tr>
          <tr>
            <td className="p-2 font-bold">Nama Pembuat</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.data.user.username}</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2 font-bold">Tanggal Request</td>
            <td className="p-2">{!value ? 'Menampilkan...' : value.data.created_date.substring(0, 10)}</td>
          </tr>
          <tr>
            <td className="p-2 font-bold">Dana Tersedia</td>
            <td className="p-2">{!value ? 'Menampilkan...' : currency(value.totaldonateafter5persen) }</td>
          </tr>
          <tr className="bg-green-150">
            <td className="p-2 font-bold">Dana Diambil</td>
            <td className="p-2">
              {!value ? 'Menampilkan...' : currency(value.totaldonate)}
            </td>
          </tr>
          <tr>
            <td className="p-2 font-bold">Rekening Tujuan</td>
            <td className="p-2">
              {!value ? 'Menampilkan...' : value.data.norek}
            </td>
          </tr>
        </tbody>
      </table>
      <div className="flex items-center justify-center text-white gap-20">
        <button type="button" onClick={accept} className="hover:bg-blue-200 bg-blue-400 py-3 px-20 rounded">Terima</button>
        <button type="button" onClick={decline} className="hover:bg-red-200 bg-red-400 py-3 px-20 rounded">Tolak</button>
      </div>
    </div>
  );
}
