import React from 'react';
import {render, fireEvent, screen} from '@testing-library/react';
import LoginContainer from '../LoginContainer';
import { act } from 'react-dom/test-utils';

describe('Login Testing', () => {
  beforeEach(() => {
    render(
      <LoginContainer />,
    );
  });

  test('login page have label Email', () => {
    const label = screen.getByText('Email');
    expect(label).toBeInTheDocument();
  });

  test('Input username and Password', async () => {
    const inputUsername = screen.getByPlaceholderText('Email Anda');
    const inputPassword = screen.getByPlaceholderText('Kata Sandi Anda');
    await act(async () => {
      fireEvent.click(inputUsername);
      fireEvent.change(inputUsername, { target: {value: 'tobantoo.adm@gmail.com'}});
      fireEvent.click(inputPassword);
      fireEvent.change(inputPassword, { target: {value: 'password' } });
    });
    expect(inputUsername).toHaveValue('tobantoo.adm@gmail.com');
    expect(inputPassword).toHaveValue('password');
  });

  test('Validation Email', async () => {
    const inputUsername = screen.getByPlaceholderText('Email Anda');
    await act(async () => {
      fireEvent.click(inputUsername);
      fireEvent.change(inputUsername, { target: {value: ''}});
      fireEvent.blur(inputUsername);
    });
    const value = await screen.getByText("Harap Isi Email");
    expect(value).toBeInTheDocument("Harap Isi Email");
  });

  test('Validation Password', async () => {
    const inputPassword = screen.getByPlaceholderText('Kata Sandi Anda');
    await act(async () => {
      fireEvent.click(inputPassword);
      fireEvent.change(inputPassword, { target: {value: ''}});
      fireEvent.blur(inputPassword);
    });
    const value = await screen.getByText("Harap Isi Kata Sandi");
    expect(value).toBeInTheDocument("Harap Isi Kata Sandi");
  });
});
