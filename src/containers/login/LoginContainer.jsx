/* eslint-disable import/no-unresolved */
import { Formik, Field } from 'formik';
import { useState } from 'react';
import Image from 'next/image';
import * as Yup from 'yup';
import { EyeIcon, EyeOffIcon } from '@heroicons/react/solid';
import { ExclamationIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';
import UserRepository from '../../libraries/repositories/user';

const validationSchema = Yup.object({
  username: Yup.string().required('Harap Isi Email'),
  password: Yup.string().required('Harap Isi Kata Sandi'),
});

const initialValues = {
  username: '',
  password: '',
};

export default function LoginContainer() {
  const router = useRouter();
  const [state, setState] = useState({
    loading: false,
    passwordShow: false,
    icon: null,
    message: '',
  });
  const handleOnSubmit = async (values, {resetForm}) => {
    setState({...state, loading: true});
    try {
      const results = await UserRepository.login(values.username, values.password);

      setTimeout(() => {
        setState({...state, loading: false});
        if (results) {
          localStorage.setItem('user', results.access_token);
          router.push('/loading');
        }
      }, 1000);
    } catch (error) {
      localStorage.removeItem('user');
      setState({...state, icon: <ExclamationIcon className="ml-2 w-5 h-5" />, message: 'Email/kata sandi salah. Periksa kembali email/kata sandi anda.'});
      resetForm(initialValues);
    }
  };

  const togglePassword = () => {
    setState({...state, passwordShow: !state.passwordShow});
  };
  return (
    <div className="md:flex w-full h-full">
      {/* Section Form */}
      <div className="md:w-5/12 shadow-2xl md:absolute md:opacity-95 md:z-10 h-screen bg-gray-100 my-auto">
        <Formik
          initialValues={initialValues}
          onSubmit={handleOnSubmit}
          validationSchema={validationSchema}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            touched,
            errors,
          }) => (
            <form className="relative p-10" onSubmit={handleSubmit}>
              {/* Section Logo */}
              <div className="mt-2 mb-10 text-center">
                <Image src="/Logo.png" className="w-full" alt="Logo App" width={60} height={60} />
                <h1 className="text-3xl font-bold">Masuk</h1>
                <p className="font-light">Masuk kembali ke akun anda</p>
              </div>
              {/* Section Logo */}
              <div className="valid-user">
                <label className="flex flex-col" htmlFor="username">
                  <span className="text-base font-medium mb-2">Email</span>
                  <Field
                    required
                    name="username"
                    id="username"
                    type="email"
                    label="Email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="p-2 rounded border-none focus:border-none outline-none focus:outline-none focus:ring-2 focus:ring-opacity-50 focus:ring-green-400"
                    placeholder="Email Anda"
                  />
                </label>
                <div className={`text-red-400 ${touched.username && errors.username ? 'error' : ''}`}>
                  {touched.username && errors.username && errors.username}
                </div>
              </div>
              <div className="my-5 valid-pass">
                <label className="flex flex-col" htmlFor="password">
                  <span className="text-base font-medium mb-2">Kata Sandi</span>
                  <div className="flex flex-row-reverse items-center">
                    <Field
                      required
                      name="password"
                      id="password"
                      label="Password"
                      type={state.passwordShow ? 'text' : 'password'}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="w-full p-2 rounded border-none focus:border-none outline-none focus:outline-none focus:ring-2 focus:ring-opacity-50 focus:ring-green-400"
                      placeholder="Kata Sandi Anda"
                    />
                    <button className="w-6 absolute mr-3" type="button" onClick={togglePassword}>{state.passwordShow ? <EyeIcon /> : <EyeOffIcon /> }</button>
                  </div>
                </label>
                <div className={`text-red-400 ${touched.password && errors.password ? 'error' : ''}`}>
                  {touched.password && errors.password && errors.password}
                </div>
              </div>
              <div className="form__group form__actions">
                <div className="text-center">
                  <div className="message text-red-400 text-sm mb-12 text-left flex gap-2">
                    {state.icon}
                    {' '}
                    {state.message}
                  </div>
                  <button type="submit" className="bg-green-650 rounded-md md:py-3 py-2 w-2/3 text-white" disabled={state.loading}>
                    {state.loading ? 'Harap Tunggu...' : 'Masuk'}
                  </button>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
      {/* Section Form */}
      {/* Section Background */}
      <div className="bg-gray-900 relative md:min-h-screen h-0 md:w-screen">
        <Image
          alt="Login"
          src="/background/bglogin.png"
          layout="fill"
          objectFit="cover"
          className=""
        />
      </div>
      {/* Section Background */}
    </div>
  );
}
