import { useState } from 'react';
import { ClipboardListIcon } from '@heroicons/react/solid';
import { ChartPieIcon, CurrencyDollarIcon } from '@heroicons/react/outline';
import Sidebar from '@/components/Sidebar';

const navigation = [
  { icon: <ChartPieIcon className="w-8 h-8 mr-3" />, name: 'Overview', href: '/superadmin' },
  { icon: <ClipboardListIcon className="w-8 h-8 mr-3" />, name: 'Campaign', href: '/superadmin/campaign' },
  { icon: <CurrencyDollarIcon className="w-8 h-8 mr-3" />, name: 'Transaction', href: '/superadmin/payment' },
];

export default function SuperAdminLayout(props) {
  const [value, setValue] = useState(props.page);
  return (
    <div className="min-h-screen">
      <div className="flex">
        <Sidebar page={value} itemNav={navigation} />
        <div className="w-full mr-11 mt-10">
          {props.children}
        </div>
      </div>
    </div>
  );
}
