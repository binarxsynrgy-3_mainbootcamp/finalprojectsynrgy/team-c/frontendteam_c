import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Navbar from '@/components/Navbar';
import UserRepository from '../../libraries/repositories/user';

const navigation = [
  { name: 'Campaign', href: '/mahasiswa/campaign' },
  { name: 'Dana', href: '/mahasiswa/payment' },
];

export default function MahasiswaLayout(props) {
  const router = useRouter();
  const [value, setValue] = useState(props.page);
  const [user, setUser] = useState({username: false});
  const loadUser = async () => {
    try {
      const token = localStorage.getItem('user');
      const guest = await UserRepository.auth(token);
      setUser(guest);
    } catch (error) {
      router.push('/login');
    }
  };
  useEffect(() => {
    loadUser();
  }, []);
  return (
    <div className="min-h-screen">
      {user.username
        ? (
          <>
            <Navbar page={value} username={user.username} itemNav={navigation} />
            <div className="w-full">
              {props.children}
            </div>
          </>
        )
        : <div />}
    </div>
  );
}
