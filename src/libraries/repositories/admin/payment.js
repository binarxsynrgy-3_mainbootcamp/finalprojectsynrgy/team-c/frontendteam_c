import { callAPI } from '../../../helpers/network';

function PaymentRepository() {
  const fetchPayment = async (token) => {
    const data = await callAPI({
      url: '/admin/history',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  const fetchDetailPayment = async (token, id) => {
    const data = await callAPI({
      url: `/admin/history/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const requestPayment = async (token, id, values) => {
    const data = await callAPI({
      url: `/admin/history/request/${id}`,
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: values,
    });
    return data;
  };

  return {
    fetchPayment,
    fetchDetailPayment,
    requestPayment,
  };
}

export default PaymentRepository();
