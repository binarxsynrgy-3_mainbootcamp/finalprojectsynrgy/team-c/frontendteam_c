import { callAPI } from '../../../helpers/network';
// import * from 'fs';

function CampaignRepository() {
  const fetchCampaign = async (token) => {
    const data = await callAPI({
      url: '/admin/project',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  const fetchDetailCampaign = async (token, id) => {
    const data = await callAPI({
      url: `/admin/project/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  const requestCampaign = async (token, values) => {
    const data = await callAPI({
      url: '/admin/project',
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(values),
    });
    return data;
  };

  const postFile = async (token, values) => {
    const form = new FormData();
    form.append('file', values);
    const response = await callAPI({
      url: '/test/uploadFile',
      method: 'POST',
      headers: {
        // 'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      data: form,
    });
    return response;
  };

  return {
    fetchCampaign,
    requestCampaign,
    postFile,
    fetchDetailCampaign,
  };
}

export default CampaignRepository();
