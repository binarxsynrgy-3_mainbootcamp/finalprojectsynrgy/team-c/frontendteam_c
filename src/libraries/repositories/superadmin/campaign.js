import { callAPI } from '../../../helpers/network';

function CampaignRepository() {
  const fetchAllCampaign = async (token) => {
    const data = await callAPI({
      //enigmatic
      // url: '/superadmin/project/',
      url: '/superadmin/project/',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  const fetchDetailCampaign = async (token, id) => {
    const data = await callAPI({
      url: `/superadmin/project/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  const acceptCampaign = async (token, id) => {
    await callAPI({
      url: `/superadmin/project/accept/${id}`,
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const declineCampaign = async (token, id) => {
    await callAPI({
      url: `/superadmin/project/decline/${id}`,
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
  };

  return {
    acceptCampaign,
    declineCampaign,
    fetchAllCampaign,
    fetchDetailCampaign,
  };
}

export default CampaignRepository();
