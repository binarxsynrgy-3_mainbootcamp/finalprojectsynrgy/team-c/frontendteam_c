import { callAPI } from '../../../helpers/network';

function PaymentRepository() {
  const fetchAllPayment = async (token) => {
    const data = await callAPI({
      url: '/superadmin/accessmoney/',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  const fetchDetailPayment = async (token, id) => {
    const data = await callAPI({
      url: `/superadmin/accessmoney/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const acceptPayment = async (token, id) => {
    await callAPI({
      url: `/superadmin/accessmoney/accept/${id}`,
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const declinePayment = async (token, id) => {
    await callAPI({
      url: `/superadmin/accessmoney/decline/${id}`,
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
  };

  return {
    acceptPayment,
    declinePayment,
    fetchAllPayment,
    fetchDetailPayment,
  };
}

export default PaymentRepository();
