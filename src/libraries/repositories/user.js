import qs from 'qs';
import { callAPI } from '../../helpers/network';

function UserRepository() {
  const login = async (username, password) => {
    const data = await callAPI({
      url: '/oauth/token',
      method: 'POST',
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      data: qs.stringify({
        username,
        password,
        grant_type: 'password',
        client_id: 'my-client-web',
        client_secret: 'password',
      }),
    });
    return data;
  };

  const auth = async (token) => {
    const data = await callAPI({
      url: '/oauth/authenticate',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data.data;
  };

  return {
    auth,
    login,
  };
}

export default UserRepository();
