module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        blue: {
          650: '#2F80ED',
        },
        red: {
          650: '#EB5757',
        },
        green: {
          150: '#90D0C8',
          250: '#B4D9D4',
          650: '#2D9F90',
          750: '#247F73',
        },
      },
      dropShadow: {
        'xl': '0 2px 6px rgba(52, 186, 168, 0.3)',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['checked'],
      borderColor: ['checked'],
      textColor: ['checked'],
      ringOffsetWidth: ['checked'],
      ringWidth: ['checked'],
      ringColor: ['checked'],
      ringOffsetColor: ['checked'],
      ringOpacity: ['checked'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
